; $4: y_origin
; $5  x_origin
; $6  x_target
; $7  color
label draw_hline:
    tx $1 $5 ; set i = x_origin
    tx $3 $6 ;
    tx $A $4 ; set screen y pointer to y_origin
    label draw_hline_pixels:
        tx $9 $1 ; set screen x pointer to x_origin
        call set_pos
        rstore $7 $B
        inc $1
        tx $2 $3
        jneq +2
            ret
        jmp draw_hline_pixels


; $4: x_origin
; $5  y_origin
; $6  y_target
; $7  color
label draw_vline:
    tx $1 $5 ; set i = y_origin
    tx $3 $6 ;
    tx $9 $4 ; set screen x pointer to x_origin
    label draw_vline_pixels:
        tx $A $1 ; set screen x pointer to x_origin
        call set_pos
        rstore $7 $B
        inc $1
        tx $2 $3
        jneq +2
            ret
        jmp draw_vline_pixels


; $4: x_origin
; $5  y_origin
; $6  width
; $7  height
; $8  color
label _draw_filled_rect_cache:
    nop ; xo
    nop ; yo
    nop ; width
    nop ; height
    nop ; color
    nop ; counter
label draw_filled_rect:
        store $4 _draw_filled_rect_cache
        store $5 _draw_filled_rect_cache+1
        store $5 _draw_filled_rect_cache+5
        store $6 _draw_filled_rect_cache+2
        store $7 _draw_filled_rect_cache+3
        store $8 _draw_filled_rect_cache+4
        label draw_filled_rect_inner:
            load $4  _draw_filled_rect_cache+5 ;yo
            load $5 _draw_filled_rect_cache; xo
            load $6 _draw_filled_rect_cache+2; width
            add $6 $5 $6 ; width + xo = x_target
            load $7 _draw_filled_rect_cache+4 ; color
            call draw_hline
            tx $1 $4
            load $2 _draw_filled_rect_cache+1 ; load y_origin into $1
            load $3 _draw_filled_rect_cache+3 ; load height in $2
            add $2 $3 $2

            jeq +4
                inc $4
                store $4 _draw_filled_rect_cache+5
                jmp draw_filled_rect_inner
            ret

; $4: x_origin
; $5  y_origin
; $6  width
; $7  height
; $8  color
label _draw_rect_cache:
    nop ; xo
    nop ; yo
    nop ; width
    nop ; height
    nop ; color
label draw_rect:
        store $4 _draw_rect_cache
        store $5 _draw_rect_cache+1
        store $6 _draw_rect_cache+2
        store $7 _draw_rect_cache+3
        store $8 _draw_rect_cache+4

        load $4  _draw_rect_cache ;xo
        load $5 _draw_rect_cache+1; yo
        load $6 _draw_rect_cache+3; height
        add $6 $5 $6 ; height + yo = y_target
        load $7 _draw_rect_cache+4 ; color
        call draw_vline

        load $4  _draw_rect_cache ;xo
        load $8 _draw_rect_cache+2 ; width
        add $8 $4 $4; xo + width = xo
        load $5 _draw_rect_cache+1; yo
        load $6 _draw_rect_cache+3; height
        add $6 $5 $6 ; height + yo = y_target
        load $7 _draw_rect_cache+4 ; color
        call draw_vline

        load $4  _draw_rect_cache+1 ;yo
        load $5 _draw_rect_cache; xo
        load $6 _draw_rect_cache+2; width
        add $6 $5 $6 ; width + xo = x_target
        load $7 _draw_rect_cache+4 ; color
        call draw_hline

        load $4  _draw_rect_cache+1 ;yo
        load $8 _draw_rect_cache+3 ; width
        add $8 $4 $4; yo + height = yo
        load $5 _draw_rect_cache; xo
        load $6 _draw_rect_cache+2; width
        add $6 $5 $6 ; width + xo = x_target
        load $7 _draw_rect_cache+4 ; color
        call draw_hline
        ret


label draw_image:
    rload $4 $7 ; width
    inc $4
    rload $4 $8 ; height
    inc $4
    loadi $3 $0
    loadi $6 $0
    label draw_pixels:
        rload $4 $5
        rstore $5 $B
        inc $B; Increment VBuf Pointer
        inc $4
        inc $3
        tx $2 $3
        tx $1 $7
        jneq +10
            loadi 3 $280
            sub $3 $7 $3
            add $3 $B $B ; Move VBUF
            loadi 3 $0
            inc $6
            tx $1 $8
            tx $2 $6
            jneq +2
                ret
        jmp draw_pixels