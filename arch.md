# Op Code Structure

## Type 1 Register and Memory

Opcode | Register | Address Mask 
-------|----------|---------
F      | F        | FFFFFF

## Type 2 Register and Register

Opcode | Register | Address Mask
-------|----------|---------
F      | F        | 00000F

## Type 3 Meta

Opcode | Register | Address Mask
-------|----------|---------
F      | F        | 000000

# Meta Instructions

Opcode | Type | Descriptions
-------|------|-------
0x00   |  3   |NOP

# 0x1 Store

Opcode | Type | Descriptions
-------|------|------------------
0x1N   | 1    | Store word in Register N into Addr

# 0x2 Load

Opcode | Type | Descriptions
-------|------|------------------
0x2N   | 1    | Load word @ Addr into Register 

# 0x3 Load Immediate

Opcode | Type | Descriptions
-------|------|------------------
0x3N   | 1    | Load Addr into Register N 


# 0x9 Register Move (Transfer)

Opcode | Type | Descriptions
-------|------|------------------
0x7N00000R   | 2    | Load word in Register `R` into Register `N` 