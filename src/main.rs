extern crate minifb;

use boo::assembler::ImageAssembler;
use boo::{Machine, REGISTER_PAGE};
use minifb::{Key, KeyRepeat, Scale, Window, WindowOptions};
use std::env;
use std::time::Duration;

const WIDTH: usize = 640;
const HEIGHT: usize = 480;

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut machine = Machine::new();
    if args.len() == 3 {
        match args[1].as_str() {
            "assemble" => {
                let mut ia = ImageAssembler::new();
                ia.assemble(&args[2]);
                machine = ia.extract();
            }
            _ => {
                panic!("unknown command {}", args[1])
            }
        }
    }

    let mut opts = WindowOptions::default();
    opts.scale = Scale::X1;

    let mut window = Window::new("Test - ESC to exit", WIDTH, HEIGHT, opts).unwrap_or_else(|e| {
        panic!("{}", e);
    });

    while window.is_open() && !window.is_key_down(Key::Escape) {
        machine.cycle();

        if machine.memory[REGISTER_PAGE + 63] == 0x01 {
            // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
            window
                .update_with_buffer(&machine.memory, WIDTH, HEIGHT)
                .unwrap();
            machine.memory[REGISTER_PAGE + 63] = 0x00;

            match window.get_keys_pressed(KeyRepeat::No) {
                None => {}
                Some(keys) => {
                    match keys.get(0) {
                        None => {
                            //machine.set_keypress(0)
                        }
                        Some(key) => match key {
                            Key::Key0 => machine.set_keypress('0' as u32),
                            Key::Key1 => machine.set_keypress('1' as u32),
                            Key::Key2 => machine.set_keypress('2' as u32),
                            Key::Key3 => machine.set_keypress('3' as u32),
                            Key::Key4 => machine.set_keypress('4' as u32),
                            Key::Key5 => machine.set_keypress('5' as u32),
                            Key::Key6 => machine.set_keypress('6' as u32),
                            Key::Key7 => machine.set_keypress('7' as u32),
                            Key::Key8 => machine.set_keypress('8' as u32),
                            Key::Key9 => machine.set_keypress('9' as u32),
                            Key::A => machine.set_keypress('A' as u32),
                            Key::B => machine.set_keypress('B' as u32),
                            Key::C => machine.set_keypress('C' as u32),
                            Key::D => machine.set_keypress('D' as u32),
                            Key::E => machine.set_keypress('E' as u32),
                            Key::F => machine.set_keypress('F' as u32),
                            Key::G => machine.set_keypress('G' as u32),
                            Key::H => machine.set_keypress('H' as u32),
                            Key::I => machine.set_keypress('I' as u32),
                            Key::J => machine.set_keypress('J' as u32),
                            Key::K => machine.set_keypress('K' as u32),
                            Key::L => machine.set_keypress('L' as u32),
                            Key::M => machine.set_keypress('M' as u32),
                            Key::N => machine.set_keypress('N' as u32),
                            Key::O => machine.set_keypress('O' as u32),
                            Key::P => machine.set_keypress('P' as u32),
                            Key::Q => machine.set_keypress('Q' as u32),
                            Key::R => machine.set_keypress('R' as u32),
                            Key::S => machine.set_keypress('S' as u32),
                            Key::T => machine.set_keypress('T' as u32),
                            Key::U => machine.set_keypress('U' as u32),
                            Key::V => machine.set_keypress('V' as u32),
                            Key::W => machine.set_keypress('W' as u32),
                            Key::X => machine.set_keypress('X' as u32),
                            Key::Y => machine.set_keypress('Y' as u32),
                            Key::Z => machine.set_keypress('Z' as u32),
                            Key::Space => machine.set_keypress(' ' as u32),
                            Key::Backspace => machine.set_keypress(0x7F as u32),
                            Key::Enter => machine.set_keypress(0x0A as u32),
                            _ => machine.set_keypress('!' as u32),
                        },
                    };
                }
            };
        }
    }
}
