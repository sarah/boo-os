        label draw_char_pixels:

            shift 5 2 1; Shift 5 by count in R(2)

            loadi 6 1;
            and 1 6 1; Mask with 0x01

            loadi 6 $FFFFFF
            mul 1 6 1;

            tx $C $2
            loadi $2 0

            jeq +2
                rstore 1 $B; Transfer RAX to the Address Stored in R11

            tx $2 $C

            inc $B; Increment VBuf Pointer

            inc 2; INC Scratch Pixel
            loadi 1 8; For Every Pixel in this Row of 8
            jneq draw_char_pixels
            ret

        label draw_char:
            rload 4 3; Load Arg 1 in 3

            loadi 6 $18;
            shift 3 6 5; Shift To First Byte
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            loadi 6 $10;
            shift 3 6 5; Shift To Second Byte
            loadi 6 $0000FF;
            and 6 5 5; Mask Off
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            loadi 6 $8;
            shift 3 6 5; Shift To Third Byte
            loadi 6 $0000FF;
            and 6 5 5;
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            loadi 6 $0;
            shift 3 6 5; Shift To 4th Byte
            loadi 6 $0000FF;
            and 6 5 5; MASK off last Byte
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            inc 4;  Move to Second Part
            rload 4 3; Load Arg 1 in 3

            loadi 6 $18;
            shift 3 6 5; Shift To First Byte
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            loadi 6 $10;
            shift 3 6 5; Shift To Second Byte
            loadi 6 $0000FF;
            and 6 5 5; Mask Off
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            loadi 6 $8;
            shift 3 6 5; Shift To Third Byte
            loadi 6 $0000FF;
            and 6 5 5;
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels

            loadi 6 $278
            add $6 $B $B ; Move VBUF

            loadi 6 $0;
            shift 3 6 5; Shift To 4th Byte
            loadi 6 $0000FF;
            and 6 5 5; MASK off last Byte
            loadi 2 0; Set Pixel to 8
            call draw_char_pixels
            ret


    label _scratch_draw_string:
            nop

    label draw_string:
             label draw_next_char:
                rload 4 3; Load Arg 1 in 3
                store 4 _scratch_draw_string

                loadi $2 $20 ; Load Addr of [SPACE] in $2
                sub $3 $2 $1 ; Sub the Character From Space

                loadi $2 2
                mul $1 $2 $1 ; Mull By 2

                loadi $2 char_space
                add $2 $1 $1 ; Add to Char Space

                tx $4 $1

                call draw_char; Call draw_char

                loadi $2 8
                add $2 $9 $9; // X = X + 9
                call set_pos

                  load $4 _scratch_draw_string
                  inc $4;
                  rload 4 3; Load Arg 1 in 3
                  tx $2 $3
                  loadi $1 0
                  jneq draw_next_char
                  ret


label num_to_charset:
        loadi $2 HEX_TABLE
        add $2 $4 $4
        rload 4 3; Load Arg 1 in 3
        loadi $2 $20 ; Load Addr of [SPACE] in $2
        sub $3 $2 $1 ; Sub the Character From Space
        loadi $2 2
        mul $1 $2 $1 ; Mull By 2
        loadi $2 char_space
        add $2 $1 $1 ; Add to Char Space
        tx $4 $1
        ret

label num_to_char:
        loadi $2 HEX_TABLE
        add $2 $4 $4
        rload 4 3; Load Arg 1 in 3
        loadi $2 $20 ; Load Addr of [SPACE] in $2
        sub $3 $2 $1 ; Sub the Character From Space
        ;loadi $2 2
        ;mul $1 $2 $1 ; Mull By 2
        ;loadi $2 char_space
        ;add $2 $1 $1 ; Add to Char Space
        tx $4 $3
        ret

; $4 contains pointer to a null terminated hex string
; outputs num in $1
label string_to_num:
    loadi $3 0
    loadi $5 4 ; bits to shift each time

    label string_to_num_inner:
        rload $4 $1 ; load char at $4 into $1
        loadi $2 $0 ; check that it isn't a null terminator

        jneq +3
            tx $1 $3
            ret

        ; check this is a valid char
        loadi $2 20
        jgt +3
            loadi $1 0
            ret

        ; sub 0x30 from char to get a normalized number
        loadi $2 $30
        sub $1 $2 $1

        loadi $2 $A
        jlt +3
            loadi $2 $7
            sub $1 $2 $1

        lshift $3 $5 $3
        add $1 $3 $3
        inc $4

        jmp string_to_num_inner
    ret