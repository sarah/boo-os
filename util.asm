label _scratch_print_var:
        nop

label print_addr:
        rload 4 3; Load Arg 1 in 3
        store $3 _scratch_print_var ; preserve rax
        loadi $7 $1C

        label print_rax_digit:
            loadi $2 8
            add $2 $9 $9; // X = X + 9
            call set_pos

            load $3 _scratch_print_var
            shift $3 $7 $4
            loadi $2 $00000F
            and $2 $4 $4

            call num_to_charset;
            call draw_char;

            loadi $2 4
            sub $7 $2 $7
            tx $2 $7
            loadi $1 0
            jneq print_rax_digit

            loadi $2 8
            add $2 $9 $9; // X = X + 9
            call set_pos

            load $3 _scratch_print_var
            shift $3 $7 $4
            loadi $2 $00000F
            and $2 $4 $4

            call num_to_charset;
            call draw_char;
        ret