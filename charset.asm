label char_space:
        $00000000
        $00000000
label char_!:
        $183c3c18
        $18001800
label char_":
        $36360000
        $00000000
label char_#:
        $36367f36
        $7f363600
label char_$:
        $0c3e031e
        $301f0c00
label char_%:
        $00633318
        $0c666300
label char_&:
        $1c361c6e
        $3b336e00
label char_':
        $06060300
        $00000000
label char_(:
        $180c0606
        $060c1800
label char_):
        $060c1818
        $180c0600
label char_*:
        $00663cff
        $3c660000
label char_+:
        $000c0c3f
        $0c0c0000
label char_,:
        $00000000
        $000c0c06
label char_-:
        $0000003f
        $00000000
label char_.:
        $00000000
        $000c0c00
label char_/:
        $6030180c
        $06030100
label char_0:
        $3e63737b
        $6f673e00
label char_1:
        $0c0e0c0c
        $0c0c3f00
label char_2:
        $1e33301c
        $06333f00
label char_3:
        $1e33301c
        $30331e00
label char_4:
        $383c3633
        $7f307800
label char_5:
        $3f031f30
        $30331e00
label char_6:
        $1c06031f
        $33331e00
label char_7:
        $3f333018
        $0c0c0c00
label char_8:
        $1e33331e
        $33331e00
label char_9:
        $1e33333e
        $30180e00
label char_::
        $000c0c00
        $000c0c00
label char_;:
        $000c0c00
        $000c0c06
label char_<:
        $180c0603
        $060c1800
label char_=:
        $00003f00
        $003f0000
label char_>:
        $060c1830
        $180c0600
label char_?:
        $1e333018
        $0c000c00
label char_@:
        $3e637b7b
        $7b031e00
label char_A:
        $0c1e3333
        $3f333300
label char_B:
        $3f66663e
        $66663f00
label char_C:
        $3c660303
        $03663c00
label char_D:
        $1f366666
        $66361f00
label char_E:
        $7f46161e
        $16467f00
label char_F:
        $7f46161e
        $16060f00
label char_G:
        $3c660303
        $73667c00
label char_H:
        $3333333f
        $33333300
label char_I:
        $1e0c0c0c
        $0c0c1e00
label char_J:
        $78303030
        $33331e00
label char_K:
        $6766361e
        $36666700
label char_L:
        $0f060606
        $46667f00
label char_M:
        $63777f7f
        $6b636300
label char_N:
        $63676f7b
        $73636300
label char_O:
        $1c366363
        $63361c00
label char_P:
        $3f66663e
        $06060f00
label char_Q:
        $1e333333
        $3b1e3800
label char_R:
        $3f66663e
        $36666700
label char_S:
        $1e33070e
        $38331e00
label char_T:
        $3f2d0c0c
        $0c0c1e00
label char_U:
        $33333333
        $33333f00
label char_V:
        $33333333
        $331e0c00
label char_W:
        $6363636b
        $7f776300
label char_X:
        $6363361c
        $1c366300
label char_Y:
        $3333331e
        $0c0c1e00
label char_Z:
        $7f633118
        $4c667f00
label char_[:
        $1e060606
        $06061e00
label char_\:
        $03060c18
        $30604000
label char_]:
        $1e181818
        $18181e00
label char_^:
        $081c3663
        $00000000
label char__:
        $00000000
        $000000ff
label char_`:
        $0c0c1800
        $00000000
label char_a:
        $00001e30
        $3e336e00
label char_b:
        $0706063e
        $66663b00
label char_c:
        $00001e33
        $03331e00
label char_d:
        $3830303e
        $33336e00
label char_e:
        $00001e33
        $3f031e00
label char_f:
        $1c36060f
        $06060f00
label char_g:
        $00006e33
        $333e301f
label char_h:
        $0706366e
        $66666700
label char_i:
        $0c000e0c
        $0c0c1e00
label char_j:
        $30003030
        $3033331e
label char_k:
        $07066636
        $1e366700
label char_l:
        $0e0c0c0c
        $0c0c1e00
label char_m:
        $0000337f
        $7f6b6300
label char_n:
        $00001f33
        $33333300
label char_o:
        $00001e33
        $33331e00
label char_p:
        $00003b66
        $663e060f
label char_q:
        $00006e33
        $333e3078
label char_r:
        $00003b6e
        $66060f00
label char_s:
        $00003e03
        $1e301f00
label char_t:
        $080c3e0c
        $0c2c1800
label char_u:
        $00003333
        $33336e00
label char_v:
        $00003333
        $331e0c00
label char_w:
        $0000636b
        $7f7f3600
label char_x:
        $00006336
        $1c366300
label char_y:
        $00003333
        $333e301f
label char_z:
        $00003f19
        $0c263f00
label char_{:
        $380c0c07
        $0c0c3800
label char_|:
        $18181800
        $18181800
label char_}:
        $070c0c38
        $0c0c0700
label char_~:
        $6e3b0000
        $00000000
label char_delete:
        $00000000
        $00000000

data ascii HEX_TABLE 0123456789ABCDEF

data ascii KEYCODES 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-=+_{}[];':"<>,.?/`~|\