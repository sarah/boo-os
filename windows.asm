; x,y, width, height; title
; returns the address of the window on the heap in $4
label make_window
    tx $C $4
    tx $D $5

    loadi $4 6
    call $FFFFF0 ; allocate 6 bytes on the heap
    tx $E $4
    rstore $C $4
    inc $4
    rstore $D $4
    inc $4
    rstore $6 $4
    inc $4
    rstore $7 $4
    inc $4
    loadi $1 221122
    rstore $1 $4
    inc $4
    rstore $8 $4

    tx $4 $E
    ret

; 4 contains the address of the window

label _draw_window
    nop
    nop
    nop
    nop
label draw_window:
    tx $C $4
    rload $C $4
    store $4 _draw_window
    inc $C
    rload $C $5
    store $5 _draw_window+1
    inc $C
    rload $C $6
     store $6 _draw_window+2
    inc $C
    rload $C $7
    store $7 _draw_window+3
    inc $C
    rload $C $8
    call draw_filled_rect
    inc $C ; now points to title buffer

    load $4  _draw_window
    load $5  _draw_window+1
    load $6  _draw_window+2
    load $7  _draw_window+3
    loadi $8 020102
    call draw_rect

    load $4  _draw_window
    load $5  _draw_window+1
    loadi $1 2
    add $4 $1 $4
    loadi $1 2
    add $5 $1 $5
    load $6  _draw_window+2
    loadi $1 4
    sub $6 $1 $6
    loadi $7  $A
    loadi $8 554455
    call draw_filled_rect

    load $4  _draw_window
    load $5  _draw_window+1
    loadi $1 4
    add $4 $1 $4
    loadi $1 4
    add $5 $1 $5
    tx $9 $4
    tx $A $5
    call set_pos

    rload $C $4
    call draw_string

    ret