# Philosophy of Boo OS

> Operating System: An operating system is a collection of things that don't fit into a language. There shouldn't be one. - [Design Principles Behind Smalltalk](https://www.cs.virginia.edu/~evans/cs655/readings/smalltalk.html)

Boo OS is a meditation on recursive complexity and the curse of simplicity. In an age where computers are so powerful, it is sometimes healing to start again. 

I have often been stuck between my desire to build either an operating system, a shell, a language or an ide. None
of those categories ever quite fit the hole I was intending them to fill.

After much introspection and research I finally decided that what I really needed was something different.

Inspired by smalltalk, Boo OS is "image" based,  the entire system can be dumped and reloaded to the same state.

Eventually my plan is that Boo OS contain all the essentials necessary for **self modification**. It should be possible to (safely) modify the operating system from within the operating system and have the changes take effect.

How that will look is undecided. For now, Boo OS is coded entirely in Boo Bytecode Assembly Language and assembled
using a Rust based assembler. One of my first targets for a real application is an Assembler written in Boo Bytecode,
run on BooOS and outputting a Boo Bytecode image.

## BooVM

Boo OS is written in Boo Bytecode which is run by the Boo Virtual Machine (BooVM). BooVM and Boo Bytecode is inspired
by various retro computing architectures with minimal instructions, but a focus on integrating with the core
systems.

As such. the BooVM offers minimal integration with the outside world. Interaction is limited to:

- Reading and Writing BooVM images to/from the Host OS
- Exposing The State of the Keyboard and Mouse
- Offering a Framebuffer for Display Output
- (Provisional) Reading and Writing "Files" from the Host OS OR Loading and Interacting with a generalized
File System that can be loaded by both operating systems (i.e. some kind of virtual disk image)
  
### Why Write a Custom VM?

Why Not? This is a meditation on recursive complexity. I was already set on writing a new language and building one from the ground up on a custom instruction set seemed like a great idea.

The longer version is that I looked into many options for the base of this project. They fell into
two main categories:

The first featured existing ISA emulations (e.g. QEMU) and existing production ready language virtual machines (e.g. JVM, Lua) - I decided that using an existing architecture wasn't in the spirit of the project. Further, both of these options
would require extra work to integrate a framebuffer display and input up to the extent where I would be writing an OS
for an existing architecture. I wanted to keep the wrapping interface to the host OS as simple as possible.

The second category featured older reto architectures, emulators and the like. I rejected this approach because
there are some details I don't want to care about, like scanlines and cartridge layouts. This approach would also
make interaction with the Host OS a project in itself likely requiring forking and significant rework resulting in
a custom emulator any way.

After research, designing and implementing an assembler for some custom bytecode and building a small VM to run
it seemed to be the simplest option and provides me with a lot of freedom moving forward, and lends itself to
infinite experimentation.

## TODO List

The below represents some ideas for projects to tackle within Boo OS.

- [x] Print Strings to the FrameBuffer
    - [x] Set Framebuffer Position Helper Function
    - [x] `put_char` Helper Function
- [ ] Memory Management
    - [ ] Heap Design
    - [ ] Implement Helper Functions `alloc` and `free`
    - [ ] Memory Safety
- [X] Keyboard Input
    - [X] Designing Input Interaction (Register Based)
- [X] Create a Console Shell to Interact with the System
    - [X] At first this shell will be limited to pre-programmed commands
    - [ ] But, it should quickly all the construction of new commands from old ones
    - [ ] Finally, the shell should facilitate saving and loading of disk-images
- [ ] Write a Bootstrapped Assembler Application in Boo Bytecode Assembly
